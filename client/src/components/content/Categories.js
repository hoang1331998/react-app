import React, { useState ,useEffect}  from 'react';
import '../../assets/css/Categories.css';
//import { Link } from 'react-router-dom';
import AddCategories from '../layout/AddCategories';
import { Button} from 'reactstrap';



const Categories = () => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    
    useEffect(() => {
        fetch("http://localhost:8888/api/categories")
          .then(res => res.json())
          .then(
            (result) => {
              
              setIsLoaded(true);    
              setItems(result['data']);
            },
           
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
        }, [])
        if (error) {
            return <div>Error: {error.message}</div>;
        } 
        else if (!isLoaded) {
            return <div>Loading...</div>;
        } 
        else {
            return (
                <div className="contentPage">
                    <div className="content__title">
                        <div className="content__title--name">
                            <h4>List</h4>
                        </div>               
                    </div>
                    <div className="content">
                        <div className="table-custom">
                            <div className="content__header">
                                <h4 class="header-title">Basic Data </h4>                             
                                <AddCategories buttonLabel='Insert' className='btnInsert'/>
                            </div>
                            
                            <table id="basic-data" class="table dt-responsive nowrap" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Parent</th>
                                        <th>Slug</th>
                                        <th>Create_at</th>
                                        <th>Updated_at</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {items.length > 0 ?  items.map((item ) => (
                                    <tr>
                                        <td key={item.name}>{item.id}</td>
                                        <td> {item.name}</td>
                                        <td> {item.parent_id}</td>
                                        <td> {item.slug}</td>
                                        <td> {item.create_at}</td>
                                        <td> {item.updated_at}</td>
                                        <td><Button color="success">Edit</Button><Button color="danger">Delete</Button></td>
                                    </tr>  
                                    )) :''}                                                   
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            );
        }
}
export default Categories;
