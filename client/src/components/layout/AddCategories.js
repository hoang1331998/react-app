import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';

const url = 'http://localhost:8888/api/categories';

const AddCategories = (props) => {

  const { buttonLabel, className} = props; 
  const [modal, setModal]         = useState(false);
  const toggle = () => setModal(!modal);

  const [data,setData] = useState({
    name: '',
    parent_id:'',
    slug: ''
  });
  const [CategoryList,setCategory] = useState([]);

  function submit(e){
    e.preventDefault();
    axios.post(url,data)
    .then(res=>{
      const mydata = [...CategoryList,res.data]
      setCategory(mydata)
    })
  }
  console.log(typeof(data.parent_id));
  function handle(e){
    const newdata={...data}
    newdata[e.target.id] = e.target.value
    setData(newdata)
  }
 
  return (
    <div>
      <Button color="primary" onClick={toggle}>{buttonLabel}</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <br />
          <Form onSubmit={(e)=>submit(e)}>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input 
              onChange={(e)=> handle(e)} 
              value={data.name}
              type="text" 
              name="name" 
              id="name" placeholder="name..." />
            </FormGroup>          
            <FormGroup>
              <Label for="parent_id">Parent</Label>
              <Input 
              onChange={(e)=> handle(e)} 
              value={data.parent_id}
              type="select" name="parent_id" id="parent_id" >
                <option selected>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Input>
            </FormGroup>           
            <FormGroup>
              <Label for="slug">Slug</Label>
              <Input 
              onChange={(e)=> handle(e)} 
              value={data.slug}
              type="text" name="slug" id="slug" />
            </FormGroup>
            {/* <FormGroup> <Label for="image">File</Label> <Input  onChange={isChange}  type="file" name="image" id="image" /> </FormGroup> */}           
            <ModalFooter>
          <Button color="primary">Insert</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
          </Form>
        </ModalBody>
      
      </Modal>
    </div>
  );
}

export default AddCategories;