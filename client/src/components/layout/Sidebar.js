import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import '../../assets/css/Sidebar.css';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Link} from "react-router-dom";

  

const Sidebar = () => {
    return (   
        <div className="sideBar">
            <div className="userProfile">
                <img className="avatar" src="http://placehold.it/40x40" alt="avatar"/>
                <div className="userInfo">
                    <h4 className="userInfo__name">Hoang Vu</h4>
                    <p className="userInfo__role">Adminstrator</p>       
                </div>
                <div className="userInfo__dropdown">
                    <ExpandMoreIcon/>
                </div>
            </div>
            <div className="sideMenu">
                <Link to="/">
                <div className="sideMenu__row">
                    <div className="sideMenu__row--icon">                           
                        <AccountCircleIcon/>
                    </div>                  
                    Categories
                </div>
                </Link>
                <Link to="/user">
                <div className="sideMenu__row">
                    <div className="sideMenu__row--icon"> 
                        
                        <AccountCircleIcon/>
                    </div>                  
                    Product
                </div>
                </Link>
                <Link to="/product">
                <div className="sideMenu__row">
                    <div className="sideMenu__row--icon"> 
                        
                        <AccountCircleIcon/>
                    </div>                  
                    User
                </div>
                </Link>
            </div>            
        </div>
          
    );
}

export default Sidebar;
