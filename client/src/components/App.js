import React from 'react';
import Navbar from './layout/Navbar';
import Sidebar from './layout/Sidebar';
import Categories from './content/Categories';
import {  BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import User from './content/User';
import Product from './content/Product';

function App() {
  return (
      <Router>
         <div className="App">
        <Navbar/>
        <Sidebar/>
        <Switch>
          <Route exact path="/" component={Categories}></Route>
          <Route exact path="/user" component={User}></Route>
          <Route exact path="/product" component={Product}></Route>
        </Switch>
        </div>
      </Router>
  );
}

export default App;
