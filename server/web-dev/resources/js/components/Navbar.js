import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { divide } from 'lodash';

import SearchIcon from '@material-ui/icons/Search';
import LanguageIcon from '@material-ui/icons/Language';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import SettingsIcon from '@material-ui/icons/Settings';


function Navbar (){
    return(
        <div className="nav">
            <div className="navbar__logo">
                <h1 className="logo">HoangVu</h1>
            </div>
            <div className="navbar__right">
                <form>
                    <div className="navbar__searchForm">
                        <div className="navbar__searchIcon">
                        <SearchIcon/>
                        </div>
                        <input placeholder="Search..." className="navbar__searchInput" type="text" />
                    </div>
                </form>
                <div className="navbar__menuIcon">
                    <div className="navbar__menuIcon--item">
                        <LanguageIcon />
                    </div>
                   <div className="navbar__menuIcon--item">
                        <NotificationsNoneIcon/>
                   </div>
                   <div className="navbar__menuIcon--item">
                        <SettingsIcon/>
                   </div>
                </div>
            </div>
        </div>
    );

}

export default Navbar;

if (document.getElementById('navbar')) {
    ReactDOM.render(<Navbar />, document.getElementById('navbar'));
}
