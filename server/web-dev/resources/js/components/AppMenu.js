import React from 'react';
import ReactDOM from 'react-dom';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const AppMenu = () => {
    return (
        <div className="sideMenu">
            <div className="userInfo">
                <div className="userInfo__avatar">
                    <img className="avatar" src="http://placehold.it/40x40" alt="avatar" />
                </div>
                <div className="userInfo__media">
                    <h5 className="userInfo__name">Hoang Vu</h5>
                    <p className="userInfo__role">Adminstrator</p>
                </div>
                <div className="profileDropdownMenu">
                    <div  className="profileDropdown__icon">
                    <ExpandMoreIcon/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AppMenu;
if(document.getElementById('app-menu')){
    ReactDOM.render(<AppMenu/>,document.getElementById('app-menu'));
}
